# ZFS Decryption via Clevis

This is based on an example setup presented in [latchset/clevis#218](https://github.com/latchset/clevis/issues/218).

This setup uses [ZFS User Properties](https://openzfs.github.io/openzfs-docs/man/8/zfsprops.8.html#User_Properties) to store the JWE objects created by clevis.

This means that JWE's cannot exceed 8192 bytes. I haven't encountered this issue yet, but your milage may vary.

## Setup

- Copy `zfs-load-key-clevis@.service` to `/etc/systemd/system`
- Copy `clevis-zsh` to `/usr/local/bin`
- Load JWE into ZFS
  ```bash
  export ZFS_DATASET="zpool/dataset"
  cat zfs.key | clevis encrypt tang '{"url": "http://tang.local"}' > zfs.jwe
  zfs set latchset.clevis:decrypt=yes $ZFS_DATASET
  zfs set latchset.clevis:jwe=$(cat zfs.jwe) $ZFS_DATASET
  rm zfs.jwe
  ```
- Test everything (script will load keys in noop mode if `keystatus` is not set to `unavailable`)
- If using `zfs-mount-generator`, use the provided `override.conf` to properly setup the `zfs-load-key` service
- If not, just `systemctl enable zfs-load-key-clevis@zpool-dataset`

## TODO

Make this into a package
